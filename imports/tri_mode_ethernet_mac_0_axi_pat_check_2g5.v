//------------------------------------------------------------------------------
// File       : tri_mode_ethernet_mac_0_axi_pat_check.v
// Author     : Xilinx Inc.
// -----------------------------------------------------------------------------
// (c) Copyright 2015 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// -----------------------------------------------------------------------------
// Description:  A simple pattern checker - expects the same data pattern as generated by the pat_gen
// with the same DA/SA order (it is expected that the frames will pass through
// two address swap blocks).  the checker will first sync to the data and then
// identify any errors (this is a sticky output but the internal signal is
// per byte)
//
//------------------------------------------------------------------------------
`timescale 1ps/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module tri_mode_ethernet_mac_0_axi_pat_check_2g5 #(
  parameter DEST_ADDR     = 48'hDA0102030405,
  parameter SRC_ADDR      = 48'h5A0102030405,
  parameter MAX_SIZE      = 16'h01F4,
  parameter MIN_SIZE      = 16'h0040,
  parameter ENABLE_VLAN   = 1'b0,
  parameter VLAN_ID       = 12'h002,
  parameter VLAN_PRIORITY = 3'b010
)
(
  input           axi_tclk,
  input           axi_tresetn,
                  
  input           enable_pat_chk,

  input           tready,
  input           tvalid,
  input  [31:0]   tdata,
  input  [3:0]    tkeep,
  input           tlast,
  input           tuser,

  output          frame_error,
  output          activity_flash
);

  // Binary encoded FSM states
  localparam  LOCK_TO_SOF = 3'b000;
  localparam  IDLE        = 3'b001;
  localparam  INFO        = 3'b010;
  localparam  PACKET      = 3'b011;

  // Constant declaration
  localparam  VLAN_LT_VAL          = 16'h8100;
  localparam  OVERHEAD_LEN_NOVLAN  = 16'h0012;
  localparam  OVERHEAD_LEN_VLAN    = 16'h0016;

  // Signals for internal use
  reg   [3:0]       rx_state;
  reg   [3:0]       nxt_rx_state;

  wire              axi_treset;
  reg               tvalid_d1;
  reg               tready_d1;
  reg   [31:0]      tdata_d1;
  reg   [31:0]      tdata_d2;
  reg               tlast_d1;
  reg               tlast_d2;
  reg   [3:0]       tkeep_d1;
  reg   [3:0]       tkeep_d2;

  wire  [31:0]      head_words        [0:3];
  wire  [31:0]      head_words_swap   [0:3];
  wire  [1:0]       head_words_term_cnt;
  reg               header_marker;
  reg               header_marker_d1;
  reg   [13:0]      cnt_head_words;
  wire  [31:0]      exp_head_data;
  wire  [31:0]      exp_head_swap_data;
  reg               flag_lt_pay_byte;
  reg               err_addr;
  reg               err_addr_swap;
  wire              lt_field_marker;
  reg               lt_field_marker_d1;
  reg               lt_field_marker_d2;
  wire  [15:0]      overhead_len_val;
  wire  [15:0]      init_lt_field_val;
  wire  [15:0]      max_lt_field_val;
  reg   [15:0]      lt_field_val;
  wire  [15:0]      exp_lt_field_val;
  wire  [15:0]      act_lt_field_val;
  reg               err_lt_field;
  reg   [7:0]       cnt_payload_data;
  wire  [7:0]       cnt_payload_data_plus_3;
  wire  [7:0]       cnt_payload_data_plus_2;
  wire  [7:0]       cnt_payload_data_plus_1;
  wire  [7:0]       cnt_payload_data_plus_0;
  reg   [3:0]       tkeep_for_pay_inc;
  reg   [7:0]       cnt_payload_data_inc;
  reg   [31:0]      exp_pay_data;
  reg   [31:0]      exp_pay_data_d1;
  reg   [31:0]      tdata_last;
  reg   [31:0]      tdata_act;
  reg               payload_marker;
  reg               payload_marker_d1;
  reg               payload_marker_d2;
  reg               check_pay_enable;
  reg               check_pay_enable_d1;
  reg               check_pay_enable_d2;
  reg               check_pay_enable_d3;
  reg               err_pay_1;
  reg               err_pay_2;
  reg               err_pay;

  wire              packet_err_i;
  reg               lch_packet_err;
  wire              frame_error_i;
  reg   [15:0]      frame_activity_count;

  // Convert the resets to be active-high 
  assign axi_treset = !axi_tresetn;

  // Pipeline input signals
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tvalid_d1 <= 1'b0;
    end
    else begin 
      tvalid_d1 <= tvalid;
    end
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tready_d1 <= 1'b0;
    end
    else begin
      tready_d1 <= tready;
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tdata_d1 <= 0;
      tdata_d2 <= 0;
    end
    else begin
      tdata_d1 <= tdata;
      tdata_d2 <= tdata_d1;
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tlast_d1 <= 1'b0;
      tlast_d2 <= 1'b0;
    end
    else begin 
      tlast_d1 <= tlast;
      tlast_d2 <= tlast_d1;
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tkeep_d1 <= 0;
      tkeep_d2 <= 0;
    end
    else begin
      tkeep_d1 <= tkeep;
      tkeep_d2 <= tkeep_d1;
    end
  end 

  //-------------------------------------------------------------------------
  // Receive state machine
  //------------------------------------------------------------------------

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      rx_state <= LOCK_TO_SOF;
    end 
    else begin
      rx_state <= nxt_rx_state;
    end 
  end 

  // RX FSM next state decode logic (fully combinatorial)
  always @(rx_state, enable_pat_chk, tvalid, tlast, tready, tuser, packet_err_i) 
  begin
    case (rx_state)
      LOCK_TO_SOF : begin
        // We need to identify the start of the frame(SOF) position.
        // The Pattern generator can be switched on any time - even mid-way
        // of an incomming frame. Thus we cannot depend only on tvalid to detect SOF
        if(enable_pat_chk && tvalid && tlast && tready) begin
          nxt_rx_state <= IDLE;
        end
        else begin 
          nxt_rx_state <= LOCK_TO_SOF;
        end 
      end
      IDLE : begin
        // Look for data-valid which will indicate arrival of new frame
        if(enable_pat_chk && tvalid) begin
          nxt_rx_state <= PACKET;
        end
        else begin
          nxt_rx_state <= IDLE;
        end
      end
      PACKET : begin
        if(!enable_pat_chk) begin
          nxt_rx_state <= LOCK_TO_SOF;
        end
        else begin
          if(tvalid && tlast && tready) begin
            if(tuser || packet_err_i) begin
              nxt_rx_state <= LOCK_TO_SOF;
            end
            else begin
              nxt_rx_state <= IDLE;
            end 
          end
          else begin
            nxt_rx_state <= PACKET;
          end
        end
      end
      default : begin
        nxt_rx_state <= LOCK_TO_SOF;
      end
    endcase
  end 

  //-------------------------------------------------------------------------
  // Check frame integrity
  //------------------------------------------------------------------------
  // Header bytes array


  generate
  if(!ENABLE_VLAN) begin : gen_head_bytes_novlan
    assign head_words[0] = {DEST_ADDR[23:16], DEST_ADDR[31:24], DEST_ADDR[39:32], DEST_ADDR[47:40]};   
    assign head_words[1] = {SRC_ADDR[39:32],  SRC_ADDR[47:40],  DEST_ADDR[7:0],   DEST_ADDR[15:8]};
    assign head_words[2] = {SRC_ADDR[7:0],    SRC_ADDR[15:8],   SRC_ADDR[23:16],  SRC_ADDR[31:24]}; 
    assign head_words[3] = 32'h00000000;

    assign head_words_swap[0] = {SRC_ADDR[23:16],   SRC_ADDR[31:24],  SRC_ADDR[39:32],  SRC_ADDR[47:40]};   
    assign head_words_swap[1] = {DEST_ADDR[39:32],  DEST_ADDR[47:40], SRC_ADDR[7:0],    SRC_ADDR[15:8]};
    assign head_words_swap[2] = {DEST_ADDR[7:0],    DEST_ADDR[15:8],  DEST_ADDR[23:16], DEST_ADDR[31:24]}; 
    assign head_words_swap[3] = 32'h00000000;

    assign head_words_term_cnt = 2'b01;
  end 
  endgenerate

  generate
  if(ENABLE_VLAN) begin : gen_head_bytes_vlan
    assign head_words[0] = {DEST_ADDR[23:16], DEST_ADDR[31:24], DEST_ADDR[39:32], DEST_ADDR[47:40]};   
    assign head_words[1] = {SRC_ADDR[39:32],  SRC_ADDR[47:40],  DEST_ADDR[7:0],   DEST_ADDR[15:8]};
    assign head_words[2] = {SRC_ADDR[7:0],    SRC_ADDR[15:8],   SRC_ADDR[23:16],  SRC_ADDR[31:24]}; 
    assign head_words[3] = {VLAN_ID[7:0],     VLAN_ID[11:8],    1'b0, VLAN_PRIORITY[2:0], VLAN_LT_VAL[7:0], VLAN_LT_VAL[15:8]};

    assign head_words_swap[0] = {SRC_ADDR[23:16],   SRC_ADDR[31:24],  SRC_ADDR[39:32],  SRC_ADDR[47:40]};   
    assign head_words_swap[1] = {DEST_ADDR[39:32],  DEST_ADDR[47:40], SRC_ADDR[7:0],    SRC_ADDR[15:8]};
    assign head_words_swap[2] = {DEST_ADDR[7:0],    DEST_ADDR[15:8],  DEST_ADDR[23:16], DEST_ADDR[31:24]}; 
    assign head_words_swap[3] = {VLAN_ID[7:0],      VLAN_ID[11:8],    1'b0, VLAN_PRIORITY[2:0], VLAN_LT_VAL[7:0], VLAN_LT_VAL[15:8]};

    assign head_words_term_cnt = 2'b10;
  end
  endgenerate 

  // Counter to track frame header position
  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == LOCK_TO_SOF || tlast) begin
      header_marker <= 1'b0;
    end
    else if(nxt_rx_state == PACKET && cnt_head_words <= head_words_term_cnt) begin
      header_marker <= 1'b1;
    end
    else begin 
      header_marker <= 1'b0;
    end
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == LOCK_TO_SOF || tlast) begin
      cnt_head_words <= 0;
    end
    else if(nxt_rx_state == PACKET && tvalid && tready && header_marker) begin
      cnt_head_words <= cnt_head_words+1;
    end 
  end 

  // Expected header data
  assign exp_head_data      = head_words[cnt_head_words[1:0]];
  assign exp_head_swap_data = head_words_swap[cnt_head_words[1:0]];

  // Compare Actual frame data with Expected frame data to detect errors
  // Compare the address fields
  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == IDLE) begin
      err_addr <= 1'b0;
    end
    else if(nxt_rx_state == PACKET && tvalid && tready && header_marker) begin
      if(tdata_d1 != exp_head_data) begin
        err_addr <= 1'b1;
      end
    end
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == IDLE) begin
      err_addr_swap <= 1'b0;
    end
    else if(nxt_rx_state == PACKET && tvalid && tready && header_marker) begin
      if(tdata_d1 != exp_head_swap_data) begin
        err_addr_swap <= 1'b1;
      end
    end 
  end 

  // We need to have a reference payload L/T field and data byte from which we can start comparision
  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == LOCK_TO_SOF) begin
      flag_lt_pay_byte <= 1'b0;
    end 
    else if(rx_state == PACKET && tvalid && tready && lt_field_marker) begin
      flag_lt_pay_byte <= 1'b1;
    end 
  end 

  // Compare L/T field with Actual frame length
  // The packet generator inserts the payload length in L/T field. Every successive 
  // frame length is one byte more than the previous frame sent till it wraps over at 
  // MAX_SIZE. To verify this field we have to maintain a counter to keep track of 
  // packet lengths over successive frames.

  // L/T field  marker
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      header_marker_d1 <= 1'b0;
    end
    else if(tready && tvalid && !tuser) begin 
      header_marker_d1 <= header_marker;
    end 
  end 

  assign lt_field_marker = (!header_marker) && header_marker_d1 && tready && tvalid;

  // Generate delayed versions of signal lt_field_marker
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      lt_field_marker_d1 <= 1'b0;          
      lt_field_marker_d2 <= 1'b0;
    end
    else begin
      lt_field_marker_d1 <= lt_field_marker;          
      lt_field_marker_d2 <= lt_field_marker_d1;
    end 
  end 

  // Packet Size counter
  generate
  if(ENABLE_VLAN) begin : gen_overhead_val_vlan
    assign overhead_len_val  = OVERHEAD_LEN_VLAN;
    assign init_lt_field_val = MIN_SIZE-OVERHEAD_LEN_VLAN;
    assign max_lt_field_val  = MAX_SIZE-OVERHEAD_LEN_VLAN;
  end
  endgenerate 

  generate
  if(!ENABLE_VLAN) begin : gen_overhead_val_novlan
    assign overhead_len_val  = OVERHEAD_LEN_NOVLAN;
    assign init_lt_field_val = MIN_SIZE-OVERHEAD_LEN_NOVLAN;
    assign max_lt_field_val  = MAX_SIZE-OVERHEAD_LEN_NOVLAN;
  end 
  endgenerate 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      lt_field_val <= init_lt_field_val;
    end 
    else if(!flag_lt_pay_byte) begin
      // At the beginnig latch the L/T field value which will form the 
      // reference value for future comparisions
      lt_field_val <= {tdata_d1[7:0], tdata_d1[15:8]};
    end  
    else if(lt_field_marker) begin
      lt_field_val <= exp_lt_field_val;
    end 
  end 

  assign exp_lt_field_val = (lt_field_val <= max_lt_field_val) ? lt_field_val+1 : init_lt_field_val;

  assign act_lt_field_val = {tdata_d1[7:0], tdata_d1[15:8]};                      

  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == IDLE) begin
      err_lt_field <= 1'b0;
    end
    else if(flag_lt_pay_byte && lt_field_marker) begin
      if(act_lt_field_val != exp_lt_field_val) begin
        err_lt_field <= 1'b1;
      end
    end
  end 
                       
  // Compare Expected payload data with Actual frame paylaod
  // The packet generator inserts the payload as a sequential number. Every successive 
  // payload byte value is one more than the previous till it wraps over. 
  // To verify this field we have to maintain a counter to keep track of 
  // payload bytes over successive frames.

  // Counter to generate the payload data bytes
  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == LOCK_TO_SOF) begin
      cnt_payload_data <= 0;
    end
    else if(!flag_lt_pay_byte) begin
      // Initialize the payload counter with the first valid farme's first payload byte
      cnt_payload_data <= tdata_d1[23:16]+2; 
    end
    else if((rx_state == PACKET && (lt_field_marker || payload_marker)) || tlast_d1) begin
      cnt_payload_data <= cnt_payload_data+cnt_payload_data_inc;
    end
  end 

  always @(tkeep_d1 or tlast_d1 or lt_field_marker)
  begin
    if(tlast_d1) begin
      tkeep_for_pay_inc <= tkeep_d1;
    end 
    else if(lt_field_marker) begin
      tkeep_for_pay_inc <= 4'b0011;
    end 
    else begin
      tkeep_for_pay_inc <= 4'b1111;
    end 
  end  

  always @(tkeep_for_pay_inc)
  begin
    case (tkeep_for_pay_inc)
      4'b0001 : cnt_payload_data_inc <= 8'h01; 
      4'b0011 : cnt_payload_data_inc <= 8'h02; 
      4'b0111 : cnt_payload_data_inc <= 8'h03; 
      default : cnt_payload_data_inc <= 8'h04; 
    endcase
  end  

  // Expected paylaod byte values
  assign cnt_payload_data_plus_3 = cnt_payload_data+3;
  assign cnt_payload_data_plus_2 = cnt_payload_data+2;
  assign cnt_payload_data_plus_1 = cnt_payload_data+1;
  assign cnt_payload_data_plus_0 = cnt_payload_data+0;

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      exp_pay_data <= 0;
    end
    else if(lt_field_marker) begin
      exp_pay_data <= {cnt_payload_data_plus_1, cnt_payload_data_plus_0, 16'h0000};
    end  
    else begin
      exp_pay_data <= {cnt_payload_data_plus_3, cnt_payload_data_plus_2, cnt_payload_data_plus_1, cnt_payload_data_plus_0};
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      exp_pay_data_d1 <= 0;
    end
    else begin
      exp_pay_data_d1 <= exp_pay_data;
    end
  end 

  // For the last word all the 4-bytes may not be valid and thus not all of them should be checked for correctness

  always @(tkeep_d2 or tdata_d2 or exp_pay_data)
  begin
    case (tkeep_d2)
      4'b0001 :tdata_last <= {exp_pay_data[31:8] , tdata_d2[7:0]}; 
      4'b0011 :tdata_last <= {exp_pay_data[31:16], tdata_d2[15:0]};
      4'b0111 :tdata_last <= {exp_pay_data[31:24], tdata_d2[23:0]};
      default :tdata_last <= tdata_d2;  
    endcase
  end  

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tdata_act <= 0;
    end 
    else if(tlast_d2) begin
      tdata_act <= tdata_last;
    end
    else begin
      tdata_act <= tdata_d2;
    end
  end 

  // Payload marker
  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == LOCK_TO_SOF || tlast) begin
      payload_marker <= 1'b0;
    end 
    else if(lt_field_marker) begin
      payload_marker <= 1'b1;
    end
  end 

  // Genarate delayed versions of payload_marker signal
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      payload_marker_d1 <= 1'b0;
      payload_marker_d2 <= 1'b0; 
    end
    else begin
      payload_marker_d1 <= payload_marker;
      payload_marker_d2 <= payload_marker_d1; 
    end 
  end 

  // Compare the incomming payload data with the expected data and raise a error flag if they differ
  // The conditions are broken down to many registerd processes to ease timing
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      check_pay_enable <= 1'b0;
    end
    else if(flag_lt_pay_byte && nxt_rx_state == PACKET && tvalid_d1 && tready_d1) begin  
      check_pay_enable <= 1'b1;
    end
    else begin
      check_pay_enable <= 1'b0;
    end
  end 

  // Generate delayed versions of signal check_pay_enable 
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      check_pay_enable_d1 <= 1'b0;
      check_pay_enable_d2 <= 1'b0;
      check_pay_enable_d3 <= 1'b0;
    end
    else begin
      check_pay_enable_d1 <= check_pay_enable;
      check_pay_enable_d2 <= check_pay_enable_d1;
      check_pay_enable_d3 <= check_pay_enable_d2;
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      err_pay_1 <= 1'b0;
    end 
    else if(lt_field_marker_d2 && tdata_act[31:16] != exp_pay_data_d1[31:16]) begin
      err_pay_1 <= 1'b1;
    end
    else begin
      err_pay_1 <= 1'b0;
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      err_pay_2 <= 1'b0;
    end
    else if(payload_marker_d2 && tdata_act != exp_pay_data_d1) begin
      err_pay_2 <= 1'b1;
    end
    else begin
      err_pay_2 <= 1'b0;
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_rx_state == IDLE) begin
      err_pay <= 1'b0;
    end
    else if(check_pay_enable_d3 && (err_pay_1 || err_pay_2)) begin
      err_pay <= 1'b1;
    end
  end 

  // Error signal is triggered by any of the frame error signals
  assign packet_err_i = (err_addr && err_addr_swap) || err_lt_field || err_pay;

  // Latch the error signal till cleared by reset
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      lch_packet_err <= 1'b0;
    end
    else if(rx_state == PACKET && !tuser && packet_err_i) begin
      lch_packet_err <= 1'b1;
    end 
  end 

  assign frame_error_i = (rx_state == PACKET) ? lch_packet_err : 1'b0;

  // need a counter for frame activity to provide some feedback that frames are being received
  // a 16 bit counter is used as this should give a slow flash rate at 10M and a very fast rate at 1G
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      frame_activity_count <= 0;
    end
    else if(tlast_d1)begin
      frame_activity_count <= frame_activity_count+1;
    end 
  end 

  // Print results in simulator
  always @(posedge axi_tclk)
  begin
    if(rx_state == PACKET && nxt_rx_state == IDLE) begin
      if(ENABLE_VLAN) begin
        if(err_addr) begin
          $display("VLAN Frame PASSED.  DA/SA swapped");
        end
        else begin
          $display("VLAN Frame PASSED.");
        end
      end
      else begin
        if(err_addr) begin
          $display("Frame PASSED.  DA/SA swapped");
        end
        else begin
          $display("Frame PASSED.");
        end
      end
    end
  end 

  // Assign to o/p ports
  assign frame_error    = frame_error_i;
  assign activity_flash = frame_activity_count[15];

endmodule



