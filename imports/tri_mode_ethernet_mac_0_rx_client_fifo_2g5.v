//------------------------------------------------------------------------------
// Title      : Receiver FIFO with AxiStream interfaces
// Version    : 1.0
// Project    : Tri-Mode Ethernet MAC
//------------------------------------------------------------------------------
// File       : tri_mode_ethernet_mac_0_rx_client_fifo_2g5.vhd
// Author     : Xilinx Inc.
// -----------------------------------------------------------------------------
// (c) Copyright 2015 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// -----------------------------------------------------------------------------
// Description: This is the receiver side FIFO for the design example
//              of the Tri-Mode Ethernet MAC core. AxiStream interfaces are used.
//
//              The FIFO is built around an Instantiated Dual Port RAM, 
//              giving a total memory capacity of 4096 bytes.
//
//              Frame data received from the MAC receiver is written into the
//              FIFO on the rx_mac_aclk. An end-of-frame marker is written to
//              the BRAM parity bit on the last byte of data stored for a frame.
//              This acts as frame deliniation.
//
//              The rx_axis_mac_tvalid, rx_axis_mac_tlast, and rx_axis_mac_tuser signals
//              qualify the frame. A frame which ends with rx_axis_mac_tuser asserted
//              indicates a bad frame and will cause the FIFO write address
//              pointer to be reset to the base address of that frame. In this
//              way the bad frame will be overwritten with the next received
//              frame and is therefore dropped from the FIFO.
//
//              Frames will also be dropped from the FIFO if an overflow occurs.
//              If there is not enough memory capacity in the FIFO to store the
//              whole of an incoming frame, the write address pointer will be
//              reset and the overflow signal asserted.
//
//              When there is at least one complete frame in the FIFO,
//              the 32-bit AxiStream read interface's rx_axis_fifo_tvalid signal will
//              be enabled allowing data to be read from the FIFO.
//
//              The FIFO has been designed to operate with different clocks
//              on the write and read sides. As the read data-width is 4 times that of 
//              the write data-width, the read clock (user side) can be upto 4-times slower
//               frequency than the write clock (MAC side).
//
//              The FIFO is designed to work with a minimum frame length of 8
//              bytes.
//
//              Requirements :
//              * Minimum frame size of 8 bytes
//              * Spacing between good/bad frame signaling (encoded by
//                rx_axis_mac_tvalid, rx_axis_mac_tlast, rx_axis_mac_tuser), is at least 64
//                clock cycles
//              * Write AxiStream clock is 312.5 MHz
//              * Read AxiStream clock is upto 4-times slower then the Write Clock
//
//------------------------------------------------------------------------------
`timescale 1ps/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module tri_mode_ethernet_mac_0_rx_client_fifo_2g5 (
  // MAC-side (write-side) AxiStream interface
  input           rx_mac_aclk,
  input           rx_mac_resetn,
  input           rx_axis_mac_tvalid,
  input   [7:0]   rx_axis_mac_tdata,
  input           rx_axis_mac_tlast,
  input           rx_axis_mac_tuser,
  // User-side (read-side) AxiStream interface
  input           rx_fifo_aclk,
  input           rx_fifo_resetn,
  input           rx_axis_fifo_tready,
  output          rx_axis_fifo_tvalid,
  output  [31:0]  rx_axis_fifo_tdata,
  output          rx_axis_fifo_tlast,
  output  [3:0]   rx_axis_fifo_tkeep,
  // FIFO status and overflow indication,
  // synchronous to write-side (rx_mac_aclk) interface
  output  [3:0]   fifo_status,
  output          fifo_overflow
);

  // Constants
  localparam ADDR_WIDTH = 10;
  localparam DATA_WIDTH = 36;

  // Binary encoded Write FSM states
  localparam WR_IDLE        = 3'b000;
  localparam WRITE_FRAME    = 3'b001;
  localparam WR_OVERFLOW    = 3'b010; 
  localparam RST_WR_ADDRESS = 3'b011;
  localparam INC_FRM_CNT    = 3'b100; 

  // Binary encoded Read FSM states
  localparam RD_IDLE         = 3'b000;
  localparam WAIT_FOR_RDY    = 3'b001;
  localparam READ_FROM_FIFO  = 3'b010; 

  // Internal signals
  wire                        rx_mac_reset;
  wire                        rx_fifo_reset;

  reg                         rx_axis_mac_tvalid_d1;
  reg                         rx_axis_mac_tvalid_d2;
  reg   [7:0]                 rx_axis_mac_tdata_d1;
  reg   [7:0]                 rx_axis_mac_tdata_d2;
  reg                         rx_axis_mac_tlast_d1;
  reg                         rx_axis_mac_tlast_d2;
  reg                         rx_axis_mac_tuser_d1;
  reg                         rx_axis_mac_tuser_d2;

  reg   [2:0]                 wr_state;
  reg   [2:0]                 nxt_wr_state;

  reg   [ADDR_WIDTH-1:0]      wr_base_addr;
  reg   [1:0]                 wr_data_byte_cnt;
  reg                         wr_addr_inc_en;
  reg   [ADDR_WIDTH-1:0]      wr_addr;
  wire  [(DATA_WIDTH/8)-1:0]  wr_en;
  reg   [DATA_WIDTH-4-1-8:0]  wr_data_pack;
  reg   [(DATA_WIDTH/8)-1:0]  wr_data_parity_pack;
  reg   [(DATA_WIDTH/8)-1:0]  wr_data_parity_pack_d1;
  reg   [(DATA_WIDTH/8)-1:0]  wr_data_parity_pack_d2;
  reg   [DATA_WIDTH-1:0]      wr_data;

  reg   [2:0]                 rd_state;
  reg   [2:0]                 nxt_rd_state;

  wire                        frame_in_fifo;
  reg   [ADDR_WIDTH-1:0]      rd_addr_prev;
  wire  [ADDR_WIDTH-1:0]      rd_addr;
  wire                        rd_addr_inc;
  wire  [DATA_WIDTH-1:0]      rd_data;
  wire  [3:0]                 rd_data_parity;
  wire                        eof_detect_i;
  wire                        eof_detect;
  reg                         rx_axis_fifo_tvalid_i;
  wire  [31:0]                rx_axis_fifo_tdata_i;
  wire                        rx_axis_fifo_tlast_i;
  wire  [3:0]                 rx_axis_fifo_tkeep_i;

  reg                         wr_store_frame_tog   = 0;
  wire                        rd_store_frame_sync;
  reg                         rd_store_frame_d1    = 0;
  wire                        rd_frame_cnt_inc;
  reg                         rd_frame_cnt_dec;
  reg   [8:0]                 rd_frames            = 0;

  reg   [1:0]                 rd_addr_tog;
  reg                         update_addr_tog;
  wire                        update_addr_tog_sync;
  reg                         update_addr_tog_sync_d1;

  reg   [ADDR_WIDTH-1:0]      wr_rd_addr           = 0;
  wire  [ADDR_WIDTH:0]        wr_addr_diff_in;
  reg   [ADDR_WIDTH-1:0]      wr_addr_diff         = 0;
  reg                         wr_fifo_full;
  wire                        fifo_overflow_i;
  reg   [3:0]                 wr_fifo_status       = 0;


  // Convert the signals to active high
  assign rx_mac_reset   = !rx_mac_resetn;
  assign rx_fifo_reset  = !rx_fifo_resetn;

  // Register input signals
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      rx_axis_mac_tvalid_d1 <= 1'b0;
      rx_axis_mac_tvalid_d2 <= 1'b0;
    end  
    else begin
      rx_axis_mac_tvalid_d1 <= rx_axis_mac_tvalid;
      rx_axis_mac_tvalid_d2 <= rx_axis_mac_tvalid_d1;
    end
  end

  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      rx_axis_mac_tdata_d1 <= 8'h00;
      rx_axis_mac_tdata_d2 <= 8'h00;
    end  
    else begin
      rx_axis_mac_tdata_d1 <= rx_axis_mac_tdata;
      rx_axis_mac_tdata_d2 <= rx_axis_mac_tdata_d1;
    end
  end

  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      rx_axis_mac_tlast_d1 <= 1'b0;
      rx_axis_mac_tlast_d2 <= 1'b0;
    end  
    else begin
      rx_axis_mac_tlast_d1  <= rx_axis_mac_tlast;
      rx_axis_mac_tlast_d2  <= rx_axis_mac_tlast_d1;
    end
  end 

  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      rx_axis_mac_tuser_d1 <= 1'b0;
      rx_axis_mac_tuser_d2 <= 1'b0;
    end
    else begin
      rx_axis_mac_tuser_d1  <= rx_axis_mac_tuser;
      rx_axis_mac_tuser_d2  <= rx_axis_mac_tuser_d1;
    end
  end 

  //-------------------------------------------------------------------------
  // Write Interface: Store the packets from MAC to the BRAM
  //------------------------------------------------------------------------
  
  // Write FSM
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_state <= WR_IDLE;
    end  
    else begin
      wr_state <= nxt_wr_state;
    end
  end 

  // Write FSM next state decode logic (fully combinatorial)
  always @(wr_state, rx_axis_mac_tvalid_d1, wr_fifo_full, rx_axis_mac_tlast_d1, rx_axis_mac_tuser_d1)
  begin
    case (wr_state)  
      WR_IDLE : begin
        if(rx_axis_mac_tvalid_d1) begin
          if(wr_fifo_full) begin
            nxt_wr_state <= WR_OVERFLOW;
          end  
          else begin
            nxt_wr_state <= WRITE_FRAME;
          end 
        end  
        else begin
          nxt_wr_state <= WR_IDLE;
        end
      end 
      WRITE_FRAME : begin
        if(wr_fifo_full) begin
          nxt_wr_state <= WR_OVERFLOW;
        end 
        else if(rx_axis_mac_tlast_d1) begin
          if(rx_axis_mac_tuser_d1) begin
            nxt_wr_state <= RST_WR_ADDRESS;
          end
          else begin
            nxt_wr_state <= INC_FRM_CNT;
          end
        end
        else begin
          nxt_wr_state <= WRITE_FRAME;
        end
      end 
      WR_OVERFLOW : begin
        if(rx_axis_mac_tlast_d1) begin
          nxt_wr_state <= RST_WR_ADDRESS;
        end
        else begin
          nxt_wr_state <= WR_OVERFLOW;
        end
      end 
      RST_WR_ADDRESS : begin
        nxt_wr_state <= WR_IDLE;
      end 
      INC_FRM_CNT : begin
        nxt_wr_state <= WR_IDLE;
      end 
      default : begin
        nxt_wr_state <= WR_IDLE;
      end 
    endcase
  end 

  
  //---------------------------------------------------------------------------------
  // FIFO write address logic
  //---------------------------------------------------------------------------------
  // Latch the packet's base address. In case the incomming farme has to be discarded
  // then the write address is reset back to the base address
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_base_addr <= 0;
    end 
    else if(nxt_wr_state == WR_IDLE) begin
      wr_base_addr <= wr_addr;
    end
  end

  // FIFO write data
  // The 8-bit Write data from MAC is packed into 32-bit word and then written to the FIFO.
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_data_pack <= 0;
    end
    else if(rx_axis_mac_tvalid_d1) begin
        case (wr_data_byte_cnt) 
           2'b00   : wr_data_pack[DATA_WIDTH-4-1-24:0]                 <= rx_axis_mac_tdata_d1; 
           2'b01   : wr_data_pack[DATA_WIDTH-4-1-16:DATA_WIDTH-4-1-23] <= rx_axis_mac_tdata_d1; 
           2'b10   : wr_data_pack[DATA_WIDTH-4-1-8 :DATA_WIDTH-4-1-15] <= rx_axis_mac_tdata_d1; 
           default : wr_data_pack <= wr_data_pack;
        endcase
    end
  end  

  // The tlast signal from MAC is used for frame de-lineation
  always @(wr_data_byte_cnt, rx_axis_mac_tlast_d1)
  begin
    case (wr_data_byte_cnt)
      2'b00   : wr_data_parity_pack <= {3'b000,                rx_axis_mac_tlast_d1};                                              
      2'b01   : wr_data_parity_pack <= {2'b00 ,                rx_axis_mac_tlast_d1, rx_axis_mac_tlast_d1};
      2'b10   : wr_data_parity_pack <= {1'b0  ,                rx_axis_mac_tlast_d1, rx_axis_mac_tlast_d1, rx_axis_mac_tlast_d1};
      default : wr_data_parity_pack <= { rx_axis_mac_tlast_d1, rx_axis_mac_tlast_d1, rx_axis_mac_tlast_d1, rx_axis_mac_tlast_d1};
    endcase
  end
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_data_parity_pack_d1 <= 0;
      wr_data_parity_pack_d2 <= 0;
    end
    else begin
      wr_data_parity_pack_d1 <= wr_data_parity_pack;
      wr_data_parity_pack_d2 <= wr_data_parity_pack_d1;
    end
  end

  // FIFO write address
  // Thus the write address pointer is incremented once every 4-RX Data Bytes OR when the EOF is reached

  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset || nxt_wr_state == WR_IDLE)  begin
      wr_data_byte_cnt <= 0;
    end  
    else if(rx_axis_mac_tvalid_d1) begin
      wr_data_byte_cnt <= wr_data_byte_cnt+1;
    end
  end

  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset)  begin
      wr_addr_inc_en <= 1'b0;
    end
    else if((rx_axis_mac_tvalid_d1 && wr_data_byte_cnt == 2'b11 && !rx_axis_mac_tlast_d1) || wr_state == INC_FRM_CNT)  begin
      wr_addr_inc_en <= 1'b1;
    end
    else begin
      wr_addr_inc_en <= 1'b0;
    end
  end 

  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset)  begin
      wr_addr <= 0;
    end
    else if(wr_state == RST_WR_ADDRESS)  begin
      // Frame is discarded - thus reset the wr_addr to value @ the start of the frame
      // so that the discarded frame can be overwritten with a new frame
      wr_addr <= wr_base_addr;
    end
    else if(wr_addr_inc_en)  begin
      wr_addr <= wr_addr+1;
    end
  end

  // FIFO write enable
  assign wr_en = {(DATA_WIDTH/8) {wr_addr_inc_en}};

  // FIFO write data
  // Write data pattern is Parity,DataByte => P3B3-P2B2-P1B1-P0B0
  // Parity bit is used for frame de-lineation
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_data <= 0;
    end
    else begin
      if(rx_axis_mac_tlast_d1) begin
        wr_data[DATA_WIDTH-1:DATA_WIDTH-1-8] <= {wr_data_parity_pack_d2[3], rx_axis_mac_tdata_d2};
      end
      else begin
        wr_data[DATA_WIDTH-1:DATA_WIDTH-1-8] <= {wr_data_parity_pack_d1[3], rx_axis_mac_tdata_d1};
      end  
      wr_data[DATA_WIDTH-10:0] <= {wr_data_parity_pack_d1[2], wr_data_pack[23:16], 
                                   wr_data_parity_pack_d1[1], wr_data_pack[15:8], 
                                   wr_data_parity_pack_d1[0], wr_data_pack[7:0]};
    end
  end 

  //-------------------------------------------------------------------------
  // Read Interface: Read-out the packets stored in FIFO 
  //------------------------------------------------------------------------

  // Read FSM
  always @(posedge rx_fifo_aclk)
  begin
    if(rx_fifo_reset) begin
      rd_state <= RD_IDLE;
    end  
    else begin
      rd_state <= nxt_rd_state;
    end
  end 

  // Read FSM next state decode logic (fully combinatorial)
  always @(rd_state, rx_axis_fifo_tready, frame_in_fifo, eof_detect)
  begin
    case (rd_state)
      RD_IDLE : begin
        if(frame_in_fifo) begin
          nxt_rd_state <= WAIT_FOR_RDY;
        end
        else begin
          nxt_rd_state <= RD_IDLE;
        end
      end  
      WAIT_FOR_RDY : begin
        if(rx_axis_fifo_tready) begin
          nxt_rd_state <= READ_FROM_FIFO;
        end
        else begin
          nxt_rd_state <= WAIT_FOR_RDY;
        end
      end  
      READ_FROM_FIFO : begin
        if(eof_detect) begin
          nxt_rd_state <= RD_IDLE;
        end
        else begin
          nxt_rd_state <= READ_FROM_FIFO;
        end
      end  
      default : begin
        nxt_rd_state <= RD_IDLE;
      end  
    endcase
  end 

  // FIFO read address logic
  always @(posedge rx_fifo_aclk)
  begin
    if(rx_fifo_reset) begin
      rd_addr_prev <= 0;
    end
    else if(nxt_rd_state == READ_FROM_FIFO || rd_state == READ_FROM_FIFO)  begin
      rd_addr_prev <= rd_addr;
    end
  end 

  assign rd_addr_inc = (rd_state == READ_FROM_FIFO) ? rx_axis_fifo_tready : 1'b0;

  assign rd_addr     = rd_addr_prev+rd_addr_inc;

  // End Of Frame (EOF) detect logic
  assign rd_data_parity[3] = rd_data[DATA_WIDTH-1];
  assign rd_data_parity[2] = rd_data[DATA_WIDTH-10];
  assign rd_data_parity[1] = rd_data[DATA_WIDTH-19];
  assign rd_data_parity[0] = rd_data[DATA_WIDTH-28];

  assign eof_detect_i = (rd_data_parity == 4'h0) ? 1'b0 : 1'b1;
  assign eof_detect   = eof_detect_i && rx_axis_fifo_tready;                    
  
  // Data valid logic
  always @(posedge rx_fifo_aclk)
  begin
    if(rx_fifo_reset) begin
      rx_axis_fifo_tvalid_i <= 1'b0;
    end
    else if(nxt_rd_state == READ_FROM_FIFO) begin
      rx_axis_fifo_tvalid_i <= 1'b1;
    end
    else begin
      rx_axis_fifo_tvalid_i <= 1'b0;
    end
  end 

  // Decode Data to FIFO
  assign rx_axis_fifo_tdata_i = {rd_data[DATA_WIDTH-2 :DATA_WIDTH-9]  , 
                                 rd_data[DATA_WIDTH-11:DATA_WIDTH-18] , 
                                 rd_data[DATA_WIDTH-20:DATA_WIDTH-27] , 
                                 rd_data[DATA_WIDTH-29:DATA_WIDTH-36]};  

  // Last word indicator logic
  assign rx_axis_fifo_tlast_i = (rd_state == READ_FROM_FIFO) ? eof_detect_i : 1'b0;

  // Data valid strobe logic 
  assign rx_axis_fifo_tkeep_i = (rx_axis_fifo_tlast_i) ? rd_data_parity : 4'b1111;

  //----------------------------------------------------------------------------
  // Frames in FIFO counter logic
  //----------------------------------------------------------------------------
  // We need to know when a frame is stored, in order to increment the count of
  // frames stored in the FIFO.
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin  
      wr_store_frame_tog <= 1'b0;
    end 
    else if(nxt_wr_state == INC_FRM_CNT) begin
      wr_store_frame_tog <= !wr_store_frame_tog;
    end 
  end

  // The frame available counter is in Read Clock domain. Thus synchronize wr_store_frame_tog
  // to Read clock domain
  tri_mode_ethernet_mac_0_sync_block resync_wr_store_frame_tog (
    .clk       (rx_fifo_aclk),
    .data_in   (wr_store_frame_tog),
    .data_out  (rd_store_frame_sync)
  );

  always @(posedge rx_fifo_aclk)
  begin
    if(rx_fifo_reset) begin
      rd_store_frame_d1 <= 1'b0;
    end
    else begin
      rd_store_frame_d1 <= rd_store_frame_sync;
    end
  end 

  assign rd_frame_cnt_inc = rd_store_frame_sync ^ rd_store_frame_d1;

  // We need to know when a frame is being read-out from the FIFO (Pulled), in order to decrement the count of
  // frames stored in the FIFO.
  always @(posedge rx_fifo_aclk)
  begin
    if(rx_fifo_reset) begin
      rd_frame_cnt_dec <= 1'b0;
    end
    else if(nxt_rd_state == READ_FROM_FIFO && rd_state == WAIT_FOR_RDY) begin
      rd_frame_cnt_dec <= 1'b1;
    end
    else begin
      rd_frame_cnt_dec <= 1'b0;
    end
  end 

  // Up/down counter to monitor the number of frames stored within the FIFO.
  // Note:
  //    * increments at the end of a frame write cycle
  //    * decrements at the beginning of a frame read cycle
  always @(posedge rx_fifo_aclk)
  begin
    if(rx_fifo_reset) begin
      rd_frames <= 0;
    end
    else begin
      // A frame is written to the FIFO in this cycle, and no frame is being
      // read out on the same cycle.
      if(rd_frame_cnt_inc && !rd_frame_cnt_dec) begin
        rd_frames <= rd_frames+1;
      // A frame is being read out on this cycle and no frame is being
      // written on the same cycle.
      end
      else if(!rd_frame_cnt_inc && rd_frame_cnt_dec) begin
        rd_frames <= rd_frames-1;
      end
    end
  end

  assign frame_in_fifo = (rd_frames !=  5'b00000) ? 1'b1 : 1'b0;

  //----------------------------------------------------------------------------
  // Overflow functionality
  //----------------------------------------------------------------------------

  // to minimise the number of read address updates the bottom 4 bits of the
  // read address are not passed across and the write domain will only sample
  // them when bits 3 and 2 of the read address transition from 01 to 10.
  // Since this is for full detection this just means that if the read stops
  // the write will hit full up to 64 locations early 

  // need to use two bits and look for an increment transition as reload can cause
  // a decrement on this boundary (decrement is only by 3 so above bit 2 should be safe)
  always @(posedge rx_fifo_aclk)
  begin
    if(rx_fifo_reset) begin
      rd_addr_tog     <= 1'b0;
      update_addr_tog <= 1'b0;
    end
    else begin
      rd_addr_tog <= rd_addr[4:3];
      if(rd_addr[4:3] == 2'b10 && rd_addr_tog == 2'b01) begin
        update_addr_tog <= !update_addr_tog;
      end
    end
  end 

  tri_mode_ethernet_mac_0_sync_block sync_rd_addr_tog (
      .clk       (rx_mac_aclk),
      .data_in   (update_addr_tog),
      .data_out  (update_addr_tog_sync)
  );

  // Obtain the difference between write and read pointers.
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      update_addr_tog_sync_d1    <= 1'b0;
      wr_rd_addr[ADDR_WIDTH-1:0] <= 0;
    end
    else begin
       update_addr_tog_sync_d1 <= update_addr_tog_sync;
       if(update_addr_tog_sync_d1 != update_addr_tog_sync) begin
         wr_rd_addr[ADDR_WIDTH-1:7] <= rd_addr[ADDR_WIDTH-1:7];
       end
    end
  end 

  assign wr_addr_diff_in = {1'b0, wr_rd_addr} - {1'b0, wr_addr};

  // Obtain the difference between write and read pointers.
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_addr_diff <= 0;
    end
    else begin
      wr_addr_diff <= wr_addr_diff_in[ADDR_WIDTH-1:0];
    end
  end

  // Detect when the FIFO is full.
  // The FIFO is considered to be full if the write address pointer is
  // within 0 to 3 of the read address pointer.
  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_fifo_full <= 1'b0;
    end
    else begin
      if(wr_addr_diff[ADDR_WIDTH-1:4] == 0 && wr_addr_diff[3:2] != 0) begin
        wr_fifo_full <= 1'b1;
      end
      else begin
        wr_fifo_full <= 1'b0;
      end
    end
  end 

  // Decode the overflow indicator output.
  assign fifo_overflow_i = (wr_state == WR_OVERFLOW) ? 1'b1 : 1'b0;

  //----------------------------------------------------------------------------
  // FIFO status signals
  //----------------------------------------------------------------------------
  // The FIFO status is four bits which represents the occupancy of the FIFO
  // in sixteenths. To generate this signal we therefore only need to compare
  // the 4 most significant bits of the write address pointer with the 4 most
  // significant bits of the read address pointer.

  always @(posedge rx_mac_aclk)
  begin
    if(rx_mac_reset) begin
      wr_fifo_status <= 0;
    end
    else begin
      if(wr_addr_diff == 0)begin
        wr_fifo_status <= 4'b0000;
      end
      else begin
        wr_fifo_status[3] <= !wr_addr_diff[ADDR_WIDTH-1];
        wr_fifo_status[2] <= !wr_addr_diff[ADDR_WIDTH-2];
        wr_fifo_status[1] <= !wr_addr_diff[ADDR_WIDTH-3];
        wr_fifo_status[0] <= !wr_addr_diff[ADDR_WIDTH-4];
      end
    end
  end 

  assign fifo_status = wr_fifo_status;
 
  //----------------------------------------------------------------------------
  // Instantiate Block RAM
  //----------------------------------------------------------------------------
  tri_mode_ethernet_mac_0_bram_tdp_2g5 #(
    .ADDR_WIDTH  (ADDR_WIDTH),
    .DATA_WIDTH  (DATA_WIDTH),
    .REGISTER_OP (0)
  )
  rx_bram (
    // Port A: Write Packets
    .a_clk  (rx_mac_aclk),
    .a_rst  (rx_mac_reset),
    .a_addr (wr_addr),
    .a_be   (wr_en),
    .a_din  (wr_data),
    // Port B: Read Packets
    .b_clk  (rx_fifo_aclk), 
    .b_rst  (rx_fifo_reset), 
    .b_addr (rd_addr), 
    .b_dout (rd_data)
  );
  
  //----------------------------------------------------------------------------
  // Assign to o/p ports
  //----------------------------------------------------------------------------
  assign rx_axis_fifo_tvalid = rx_axis_fifo_tvalid_i; 
  assign rx_axis_fifo_tdata  = rx_axis_fifo_tdata_i;
  assign rx_axis_fifo_tlast  = rx_axis_fifo_tlast_i;
  assign rx_axis_fifo_tkeep  = rx_axis_fifo_tkeep_i;   
  assign fifo_overflow       = fifo_overflow_i;

endmodule

