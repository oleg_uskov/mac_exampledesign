# PART is kintex7 xc7k410tffg676

#
####
#######
##########
#############
#################
## System level constraints




#
####
#######
##########
#############
#################
#EXAMPLE DESIGN CONSTRAINTS


############################################################
# Clock Period Constraints                                 #
############################################################

############################################################
# TX Clock period Constraints                              #
############################################################
# Transmitter clock period constraints: please do not relax

#create_clock -name clkout0 -period 3.200 [get_ports gtx_clk]
#create_clock -name axi_lite_clk -period 10.0 [get_ports axi_lite_clk]


 
#set axi_clk_name [get_clocks axi_lite_clk]

############################################################
# Input Delay constraints
############################################################
# these inputs are alll from either dip switchs or push buttons
# and therefore have no timing associated with them
#set_false_path -from [get_ports config_board]
#set_false_path -from [get_ports pause_req_s]
#set_false_path -from [get_ports reset_error]
#set_false_path -from [get_ports mac_speed[0]]
#set_false_path -from [get_ports mac_speed[1]]
#set_false_path -from [get_ports gen_tx_data]
#set_false_path -from [get_ports chk_tx_data]

#set_false_path -from [get_ports rx_asis*]
#set_false_path -from [get_ports tx_asis*]
#set_false_path -from [get_ports gmii_*]
#set_false_path -from [get_ports glbl_rst]

# no timing requirements but want the capture flops close to the IO
#set_max_delay -from [get_ports update_speed] 4 -datapath_only


# Ignore pause deserialiser as only present to prevent logic stripping
#set_false_path -from [get_ports pause_req*]
#set_false_path -from [get_cells pause_req* -filter {IS_SEQUENTIAL}]
#set_false_path -from [get_cells pause_val* -filter {IS_SEQUENTIAL}]


############################################################
# Output Delay constraints
############################################################

#set_false_path -to [get_ports frame_error]
#set_false_path -to [get_ports frame_errorn]
#set_false_path -to [get_ports serial_response]

#set_false_path -to [get_ports tx_statistics_s]
#set_false_path -to [get_ports rx_statistics_s]
#set_output_delay -clock $axi_clk_name 1 [get_ports mdc]



############################################################
# Ignore paths to resync flops
############################################################
set_false_path -to [get_pins -hier -filter {NAME =~ */reset_sync*/PRE}]



#
####
#######
##########
#############
#################
#FIFO BLOCK CONSTRAINTS

############################################################
# FIFO Clock Crossing Constraints                          #
############################################################

# control signal is synched separately so this is a false path
set_max_delay -from [get_cells -hier -filter {NAME =~ *rx_fifo_i/rx_axis_fifo_tvalid_i_reg}]              -to [get_cells -hier -filter {name =~ *fifo*wr_rd_addr_reg[*]}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {name =~ *rx_fifo_i/rd_addr_prev_reg[*]}]                    -to [get_cells -hier -filter {name =~ *fifo*wr_rd_addr_reg[*]}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {name =~ *rx_fifo_i/wr_store_frame_tog_reg}]                 -to [get_cells -hier -filter {name =~ *fifo_i/resync_wr_store_frame_tog/data_sync_reg0}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {name =~ *rx_fifo_i/update_addr_tog_reg}]                    -to [get_cells -hier -filter {name =~ *rx_fifo_i/sync_rd_addr_tog/data_sync_reg0}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {name =~ *tx_fifo_i/rd_addr_txfer_reg[*]}]                   -to [get_cells -hier -filter {name =~ *fifo*wr_rd_addr_reg[*]}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {name =~ *tx_fifo_i/rd_txfer_tog_reg}]                       -to [get_cells -hier -filter {name =~ *tx_fifo_i/resync_rd_txfer_tog/data_sync_reg0}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *tx_fifo_i/wr_store_frame_tog_reg}]                 -to [get_cells -hier -filter {NAME =~ *tx_fifo_i/resync_wr_store_frame_tog/data_sync_reg0}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {NAME =~ *tx_fifo_i/rd_txfer_tog_reg}]                       -to [get_cells -hier -filter {NAME =~ *tx_fifo_i/resync_rd_txfer_tog/data_sync_reg0}] 3.2 -datapath_only
set_max_delay -from [get_cells -hier -filter {name =~ *rx_fifo_i/rd_state_reg[*]}]                        -to [get_cells -hier -filter {name =~ */rx_fifo_i/wr_rd_addr_reg[*]}] 3.2 -datapath_only

