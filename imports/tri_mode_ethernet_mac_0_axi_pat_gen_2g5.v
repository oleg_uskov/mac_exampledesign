//------------------------------------------------------------------------------
// File       : tri_mode_ethernet_mac_0_axi_pat_gen.vhd
// Author     : Xilinx Inc.
// -----------------------------------------------------------------------------
// (c) Copyright 2015 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// -----------------------------------------------------------------------------
// Description:  This is a very simple pattern generator which will generate packets 
// with the supplied dest_addr and src_addr and incrementing data.  The packet size 
// increments between the min and max size (which can be set to the same value if a 
// specific size is required
//
//------------------------------------------------------------------------------
`timescale 1ps/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module tri_mode_ethernet_mac_0_axi_pat_gen_2g5 #(
  parameter DEST_ADDR     = 48'hDA0102030405,
  parameter SRC_ADDR      = 48'h5A0102030405,
  parameter MAX_SIZE      = 16'h01F4,
  parameter MIN_SIZE      = 16'h0040,
  parameter ENABLE_VLAN   = 1'b0,
  parameter VLAN_ID       = 12'h002,
  parameter VLAN_PRIORITY = 3'b010
)
(
  input                 axi_tclk,
  input                 axi_tresetn,

  input                 enable_pat_gen,

  input                 tready,
  output  reg           tvalid,
  output        [31:0]  tdata,
  output        [3:0]   tkeep,
  output                tlast
);

  // Binary encoded FSM states
  localparam  PREPROCESS  = 3'b000;
  localparam  IDLE        = 3'b001;
  localparam  XMIT_FRAME  = 3'b010;
  localparam  KILL_BW     = 3'b011;

  // Constant declaration
  localparam  VLAN_LT_VAL          = 16'h8100;
  localparam  OVERHEAD_LEN_NOVLAN  = 16'h0012;
  localparam  OVERHEAD_LEN_VLAN    = 16'h0016;
  // Define the number of idle cycles after each frame transfer
  localparam  KILL_BW_TERM_CNT     = 5'b11000;

  // Signals for internal use
  wire          axi_treset;

  reg   [3:0]     basic_rc_counter;
  wire            add_credit;
  reg   [12:0]    credit_count;
  reg   [15:0]    cnt_frame_words;
  reg   [4:0]     cnt_kill_bw;
  wire            cnt_kill_bw_done;
  
  reg   [3:0]     tx_state;
  reg   [3:0]     nxt_tx_state;

  wire  [15:0]    overhead_len_val;
  wire  [15:0]    init_lt_field_val;
  wire  [15:0]    max_lt_field_val;
  reg   [15:0]    cnt_payload_len;
  wire  [15:0]    cnt_payload_len_4tlast;
  wire  [15:0]    cnt_frame_len;

  wire  [31:0]    head_words  [0:3];
  wire  [1:0]     head_words_term_cnt;
  reg             head_words_xmit_done;
  reg             head_words_xmit_done_d1;
  wire            head_words_xmit_done_re;
  wire            frame_xmit_done;
  reg   [7:0]     cnt_payload_data;
  reg   [7:0]     cnt_payload_data_inc;
  wire  [7:0]     cnt_payload_data_plus_3;
  wire  [7:0]     cnt_payload_data_plus_2;
  wire  [7:0]     cnt_payload_data_plus_1;
  wire  [7:0]     cnt_payload_data_plus_0;
 
  wire  [31:0]    tdata_i;
  reg   [3:0]     tkeep_i;
  wire  [3:0]     tkeep_i1;
  wire            tlast_i;

  // Convert the resets to be active-high 
  assign axi_treset = !axi_tresetn;

  // Rate control logic
  // First we need an always active counter to provide the credit control
  always @(posedge axi_tclk)
  begin
    if(axi_treset  || !enable_pat_gen) begin
      basic_rc_counter <= 0;
    end
    else if(tx_state != PREPROCESS) begin 
      basic_rc_counter <= basic_rc_counter+1;
    end 
  end 

  // Now we need to set the compare level depending upon the selected speed
  // the credits are applied using a simple less-than check
  assign add_credit = (basic_rc_counter == 8'hF) ? 1'b1 : 1'b0;

  // basic credit counter - -ve value means do not send a frame
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      credit_count <= 0;
    end 
    else if(tx_state == XMIT_FRAME) begin
      // We are in frame
      if(!add_credit && credit_count[12:10] != 3'b110) begin 
        // Stop decrementing at -2048
        credit_count <= credit_count-1;
      end 
    end
    else if(add_credit && credit_count[12:11] != 2'b01) begin 
      // Stop incrementing at 2048
      credit_count <= credit_count+1;
    end 
  end 

  // Generate the frame words transmitted counter:
  always @(posedge axi_tclk)
  begin
    if(axi_treset || frame_xmit_done) begin
      cnt_frame_words <= 0;
    end
    else if(tx_state == XMIT_FRAME && tready) begin
      cnt_frame_words <= cnt_frame_words+4;
    end 
  end 

  //-------------------------------------------------------------------------
  // FSM to control frame generation and transmission
  //------------------------------------------------------------------------
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tx_state <= PREPROCESS;
    end  
    else begin
      tx_state <= nxt_tx_state;
    end 
  end 

  // FSM next state decode logic; Fully combinatorial
  always @(tx_state, enable_pat_gen, credit_count, tready, frame_xmit_done, cnt_kill_bw_done)
  begin
    case (tx_state)
      PREPROCESS : begin
        nxt_tx_state <= IDLE;
      end
      IDLE : begin
        if(enable_pat_gen && !credit_count[12]) begin
          nxt_tx_state <= XMIT_FRAME;
        end
        else begin
          nxt_tx_state <= IDLE;
        end 
      end
      XMIT_FRAME : begin
        if(frame_xmit_done && tready) begin
          nxt_tx_state <= KILL_BW;
        end
        else begin
          nxt_tx_state <= XMIT_FRAME;
        end
      end
      KILL_BW : begin
        if(cnt_kill_bw_done) begin
          nxt_tx_state <= IDLE;
        end
        else begin
          nxt_tx_state <= KILL_BW;
        end
      end
      default : begin
        nxt_tx_state <= IDLE;
      end
    endcase
  end

  // Kill some bandwidth by doing nothing
  always @(posedge axi_tclk)
  begin
    if(axi_treset || nxt_tx_state == IDLE) begin
      cnt_kill_bw <= 0;
    end  
    else if(nxt_tx_state == KILL_BW) begin
      cnt_kill_bw <= cnt_kill_bw+1;
    end 
  end 

  assign cnt_kill_bw_done = (cnt_kill_bw == KILL_BW_TERM_CNT) ? 1'b1 : 1'b0;

  //-------------------------------------------------------------------------
  // Frame Length Counter
  //------------------------------------------------------------------------
  generate
  if(ENABLE_VLAN) begin : gen_overhead_val_vlan 
    assign overhead_len_val  = OVERHEAD_LEN_VLAN-4-1;
    assign init_lt_field_val = MIN_SIZE-OVERHEAD_LEN_VLAN;
    assign max_lt_field_val  = MAX_SIZE-OVERHEAD_LEN_VLAN;
  end 
  endgenerate

  generate
  if(!ENABLE_VLAN) begin : gen_overhead_val_novlan
    assign overhead_len_val  = OVERHEAD_LEN_NOVLAN-4-1;
    assign init_lt_field_val = MIN_SIZE-OVERHEAD_LEN_NOVLAN;
    assign max_lt_field_val  = MAX_SIZE-OVERHEAD_LEN_NOVLAN;
  end 
  endgenerate

  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      cnt_payload_len <= init_lt_field_val;
    end
    else if(frame_xmit_done) begin
      if(cnt_payload_len <= max_lt_field_val) begin
        cnt_payload_len <= cnt_payload_len+1;
      end  
      else begin
        cnt_payload_len <= init_lt_field_val;
      end 
    end 
  end 

  //-------------------------------------------------------------------------
  // Transmit Frame and related o/p siganls generation
  //------------------------------------------------------------------------

  // Header bytes array
  generate
  if(!ENABLE_VLAN) begin : gen_head_bytes_novlan
    assign head_words[0] = {DEST_ADDR[23:16], DEST_ADDR[31:24], DEST_ADDR[39:32], DEST_ADDR[47:40]};   
    assign head_words[1] = {SRC_ADDR[39:32],  SRC_ADDR[47:40],  DEST_ADDR[7:0],   DEST_ADDR[15:8]};
    assign head_words[2] = {SRC_ADDR[7:0],    SRC_ADDR[15:8],   SRC_ADDR[23:16],  SRC_ADDR[31:24]}; 
    assign head_words[3] = 32'h00000000;

    assign head_words_term_cnt = 2'b01;
  end 
  endgenerate

  generate
  if(ENABLE_VLAN) begin : gen_head_bytes_vlan
    assign head_words[0] = {DEST_ADDR[23:16], DEST_ADDR[31:24], DEST_ADDR[39:32], DEST_ADDR[47:40]};   
    assign head_words[1] = {SRC_ADDR[39:32],  SRC_ADDR[47:40],  DEST_ADDR[7:0],   DEST_ADDR[15:8]};
    assign head_words[2] = {SRC_ADDR[7:0],    SRC_ADDR[15:8],   SRC_ADDR[23:16],  SRC_ADDR[31:24]}; 
    assign head_words[3] = {VLAN_ID[7:0],     VLAN_ID[11:8],    1'b0, VLAN_PRIORITY[2:0], VLAN_LT_VAL[7:0], VLAN_LT_VAL[15:8]};

    assign head_words_term_cnt = 2'b10;
  end 
  endgenerate

  // Signal to indicate that the frame's header bytes have been transmitted
  always @(posedge axi_tclk)
  begin
    if(axi_treset || frame_xmit_done) begin
      head_words_xmit_done <= 0;
    end   
    else if(cnt_frame_words[3:2] > head_words_term_cnt) begin
      head_words_xmit_done <= 1'b1;
    end 
  end 

  always @(posedge axi_tclk)
  begin
    if(axi_treset ) begin
      head_words_xmit_done_d1 <= 1'b0;
    end  
    else begin 
      head_words_xmit_done_d1 <= head_words_xmit_done;
    end 
  end 

  assign head_words_xmit_done_re = head_words_xmit_done && (!head_words_xmit_done_d1);
                          
  // Signal to indicate that all the words of the frame have been transmitted
  assign cnt_frame_len   = cnt_payload_len+overhead_len_val;
  assign frame_xmit_done = (cnt_frame_words == {cnt_frame_len[15:2], 2'b00}) ? 1'b1 : 1'b0;
                     
  // Counter to generate the payload data bytes
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      cnt_payload_data <= 0;
    end  
    else if(tx_state == XMIT_FRAME && tready && head_words_xmit_done) begin
      if(head_words_xmit_done_re) begin
        cnt_payload_data <= cnt_payload_data+2;
      end  
      else begin
        cnt_payload_data <= cnt_payload_data+cnt_payload_data_inc;
      end 
    end 
  end 

  always @(tkeep_i1)
  begin
    case (tkeep_i1)
      4'b0001 : cnt_payload_data_inc <= 4'h1;
      4'b0011 : cnt_payload_data_inc <= 4'h2;
      4'b0111 : cnt_payload_data_inc <= 4'h3;
      default : cnt_payload_data_inc <= 4'h4;
    endcase
  end  

  // Paylaod data bytes are sequential
  assign cnt_payload_data_plus_3 = cnt_payload_data+3;
  assign cnt_payload_data_plus_2 = cnt_payload_data+2;
  assign cnt_payload_data_plus_1 = cnt_payload_data+1;
  assign cnt_payload_data_plus_0 = cnt_payload_data;

  assign tdata_i = (head_words_xmit_done_re) ? {cnt_payload_data_plus_1, cnt_payload_data_plus_0, cnt_payload_len[7:0],    cnt_payload_len[15:8]} :
                                               {cnt_payload_data_plus_3, cnt_payload_data_plus_2, cnt_payload_data_plus_1, cnt_payload_data_plus_0};

  // Mux out the Frame's Header or Payload byte appropriatly
  assign tdata = (!head_words_xmit_done) ? head_words[cnt_frame_words[3:2]] : tdata_i;

  // Generate tvalid
  always @(posedge axi_tclk)
  begin
    if(axi_treset) begin
      tvalid <= 1'b0;
    end
    else if(nxt_tx_state == XMIT_FRAME) begin
      tvalid <= 1'b1;
    end
    else begin
      tvalid <= 1'b0;
    end 
  end 

  // Generate tlast
  assign tlast_i = (tx_state == XMIT_FRAME && frame_xmit_done) ? 1'b1 : 1'b0;

  // Generate tkeep
  // As we have already put 2 payload bytes in the frame-word which has the l/t field, subtract 2 here
  assign cnt_payload_len_4tlast = cnt_payload_len-2;

  always @(cnt_payload_len_4tlast)
  begin
    case (cnt_payload_len_4tlast[1:0])
      2'b01   : tkeep_i <= 4'b0001; 
      2'b10   : tkeep_i <= 4'b0011;
      2'b11   : tkeep_i <= 4'b0111;
      default : tkeep_i <= 4'b1111;
    endcase
  end  

  assign tkeep_i1 = (tx_state == XMIT_FRAME && tlast_i) ? tkeep_i : 4'b1111;

  // Assign to O/P ports
  assign tkeep = tkeep_i1;
  assign tlast = tlast_i;

endmodule


