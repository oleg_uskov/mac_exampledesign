//--------------------------------------------------------------------------------
// Title      : Demo testbench
// Project    : Tri-Mode Ethernet MAC
//--------------------------------------------------------------------------------
// File       : loopback_module.v
// -----------------------------------------------------------------------------
// (c) Copyright 2004-2015 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// -----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// $Revision: $
// $Date: $
// Title      :  
// Project    : 
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// File          : loop_back_module.v
// Author        : Xilinx, Inc.
// Created On    : Fri Jun 05/06/2015 03:21:24 PM IST
//-----------------------------------------------------------------------------
// Description   : 
// $Id: $
//-----------------------------------------------------------------------------
// (c) Copyright 2010 - 2011 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and 
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
//---------------------------------------------------------------------

`ifndef LOOP_BACK_MODULE_V__
`define LOOP_BACK_MODULE_V__

`timescale 1 ps/1 ps

module tri_mode_ethernet_mac_0_loopback_module (
  input               reset,
  input               gtx_clk,
  input       [7:0]   gmii_txd,
  input               gmii_tx_en,
  input               gmii_tx_er,
  output  reg [7:0]   gmii_rxd,
  output  reg         gmii_rx_dv,
  output  reg         gmii_rx_er,
  output  reg         exp_error,
  output  reg         transmit_error
);

  reg   [15:0]  length;
  reg   [15:0]  pkt_cnt;
  reg   [15:0]  byte_pos;
  reg   [15:0]  decr_data;
  reg           err_injection;
  reg           gmii_tx_en_r;
  reg   [15:0]  payload_length;
  reg           pat_gen_data_error;
  reg           pat_gen_error;
  reg           inj_error;
  reg           inj_error1;
  reg           frame_done;

  wire  [7:0]   gmii_txd1;
  wire          gmii_tx_en1;
  wire          gmii_tx_er1;

  assign gmii_txd1   = gmii_txd;
  assign gmii_tx_en1 = gmii_tx_en;
  assign gmii_tx_er1 = gmii_tx_er;

  initial begin
    inj_error = $urandom();
  end

 always @(posedge gtx_clk) 
 begin
   if(reset) begin
     gmii_tx_en_r         <= 0;
     pkt_cnt              <= 0;
     length               <= 0;
     decr_data            <= 0;
     payload_length       <= 0;
     frame_done           <= 1'b0;
     byte_pos             <= 'h0;
     err_injection        <= 1'b0;
     pat_gen_error        <= 1'b0;
     pat_gen_data_error   <= 1'b0;
     exp_error            <= 1'b0;
   end 
   else begin
     gmii_rx_dv <= gmii_tx_en1;
     gmii_rxd   <= gmii_txd1;
     //clear pat gen error at the start of the next frame
     if(!gmii_tx_en_r && gmii_tx_en1) begin
        pat_gen_data_error <= 1'b0;   
     end
     if(gmii_tx_en1) begin
        gmii_tx_en_r  <= gmii_tx_en1;        
        byte_pos      <= byte_pos+1;
     end     
     //update pkt length , pkt cnt after frame ends
     else if(gmii_tx_en_r && !gmii_tx_en1) begin
       pkt_cnt      <= pkt_cnt+1;
       length       <= byte_pos-26;
       byte_pos     <= 0;
       frame_done   <= 1'b1;
       gmii_tx_en_r <= 1'b0;
     end  
     if(frame_done) begin
       frame_done <= 1'b0;
     end  
     //insert error in 5th pkt in data byte 3 if inj_error is 1
     if(pkt_cnt == 4 && byte_pos == 16+8) begin
       if(inj_error == 1)  begin
         gmii_rx_er <= 1'b1;
         exp_error <= 1'b1;
       end else begin
         gmii_rx_er <= gmii_tx_er1;
       end  
     end else begin
       gmii_rx_er <= gmii_tx_er1;
     end
     if(byte_pos == 12+8) begin
       payload_length[15:8] <= gmii_txd1;
     end
     else if(byte_pos == 13+8) 
     begin
       payload_length[7:0] <= gmii_txd1;
     end else if((payload_length + 22 > byte_pos - 1) && byte_pos > 22) begin // 7PRE + SFD + DA +SA +L/Type + FCS = 22 + 4 bytes
       decr_data [7:0] <= decr_data[7:0] + 1; //could be incr data
       if(decr_data[7:0] != gmii_rxd) begin
         pat_gen_data_error <= 1'b1;
       end  
     end
   end
 end

 always @(posedge gtx_clk) begin
  if(frame_done == 1'b1) begin
    if(length != payload_length) begin
      pat_gen_error <= 1'b1;
    end  
    transmit_error <= pat_gen_error | pat_gen_data_error;
    if(pat_gen_error | pat_gen_data_error) begin
      $display("** ERROR: PKT Generator error seen %t",$realtime,"ps");
    end  
  end
  else if(transmit_error) 
    begin
      transmit_error <= 1'b0; //clear error signal as this is reported
      pat_gen_error <= 1'b0;
    end
 end

endmodule
`endif //LOOP_BACK_MODULE_V__
