//------------------------------------------------------------------------------
// Title      : Transmitter FIFO with AxiStream interfaces
// Version    : 1.0
// Project    : Tri-Mode Ethernet MAC
//------------------------------------------------------------------------------
// File       : tri_mode_ethernet_mac_0_tx_client_fifo_2g5.v
// Author     : Xilinx Inc.
// -----------------------------------------------------------------------------
// (c) Copyright 2015 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// -----------------------------------------------------------------------------
// Description: This is a transmitter side FIFO for the design example
//              of the Tri-Mode Ethernet MAC core. AxiStream interfaces are used.
//
//              The FIFO is built around an Instantiated Dual Port RAM, 
//              giving a total memory capacity of 4096 bytes.
//
//              Valid frame data received from the user interface is written
//              into the Block RAM on the tx_fifo_aclk. The FIFO will store
//              frames up to 4kbytes in length. If larger frames are written
//              to the FIFO, the AxiStream interface will accept the rest of the
//              frame, but that frame will be dropped by the FIFO and the
//              overflow signal will be asserted.
//
//              The FIFO is designed to work with a minimum frame length of 14
//              bytes.
//
//              When there is at least one complete frame in the FIFO, the MAC
//              transmitter AxiStream interface will be driven to request frame
//              transmission by placing the first byte of the frame onto
//              tx_axis_mac_tdata and by asserting tx_axis_mac_tvalid. The MAC will later
//              respond by asserting tx_axis_mac_tready. At this point the remaining
//              frame data is read out of the FIFO subject to tx_axis_mac_tready.
//              Data is read out of the FIFO on the tx_mac_aclk.
//
//              The FIFO has been designed to operate with different clocks
//              on the write and read sides. The minimum write clock
//              frequency is the read clock frequency divided by 2.
//
//              The FIFO memory size can be increased by expanding the rd_addr
//              and wr_addr signal widths, to address further BRAMs.
//
//------------------------------------------------------------------------------
`timescale 1ps / 1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module tri_mode_ethernet_mac_0_tx_client_fifo_2g5 (
  // User-side (write-side) AxiStream interface
  input           tx_fifo_aclk,
  input           tx_fifo_resetn,
  input   [31:0]  tx_axis_fifo_tdata,
  input   [3:0]   tx_axis_fifo_tkeep,
  input           tx_axis_fifo_tvalid,
  input           tx_axis_fifo_tlast,
  output          tx_axis_fifo_tready,
  // MAC-side (read-side) AxiStream interface
  input           tx_mac_aclk,
  input           tx_mac_resetn,
  output          tx_axis_mac_tvalid,
  output  [7:0]   tx_axis_mac_tdata,
  output          tx_axis_mac_tlast,
  input           tx_axis_mac_tready,
  output          tx_axis_mac_tuser,
  // FIFO status and overflow indication,
  // synchronous to write-side (tx_user_aclk) interface
  output          fifo_overflow,
  output  [3:0]   fifo_status
);


  // Constants
  // Address and Data bus widths
  localparam ADDR_WIDTH = 10;
  localparam DATA_WIDTH = 36;

  // Binary encoded FSM states
  localparam WR_IDLE        = 3'b000;
  localparam WRITE_FRAME    = 3'b001;
  localparam WR_OVERFLOW    = 3'b010;
  localparam INC_FRM_CNT    = 3'b011;
  localparam RST_WR_ADDRESS = 3'b100;

  localparam RD_IDLE            = 4'b0000;
  localparam WAIT_FOR_RDY       = 4'b0001;
  localparam READ_FROM_FIFO     = 4'b0010;
  localparam PREFETCH_RD_DATA_1 = 4'b0011;
  localparam PREFETCH_RD_DATA_2 = 4'b0100;
  localparam PREFETCH_RD_DATA_3 = 4'b0101;

  // Internal signals
  wire                      tx_mac_reset;
  wire                      tx_fifo_reset;

  reg                       tx_axis_fifo_tready_i;
  reg                       tx_axis_fifo_tvalid_d1;
  reg                       tx_axis_fifo_tvalid_d2;
  reg [31:0]                tx_axis_fifo_tdata_d1;
  reg [3:0]                 tkeep_to_fifo_wr;
  reg                       tx_axis_fifo_tlast_d1;
  reg                       tx_axis_mac_tvalid_i;
  reg [7:0]                 tx_axis_mac_tdata_i;
  reg                       tx_axis_mac_tlast_i;

  reg [3:0]                 wr_state;
  reg [3:0]                 nxt_wr_state;

  reg [ADDR_WIDTH-1:0]      wr_base_addr;
  wire                      wr_addr_inc_en;
  reg [ADDR_WIDTH-1:0]      wr_addr;
  reg [(DATA_WIDTH/8)-1:0]  wr_en;
  reg [DATA_WIDTH-1:0]      wr_data;
  reg [(DATA_WIDTH/8)-1:0]  wr_data_parity;

  reg [3:0]                 rd_state;
  reg [3:0]                 nxt_rd_state;

  //** modified by Abhay
  //**wire                      frame_in_fifo;
  reg                       frame_in_fifo;
  wire                      rd_addr_inc_en;
  reg [ADDR_WIDTH-1:0]      rd_addr;
  wire[DATA_WIDTH-1:0]      rd_data_with_parity;
  wire[DATA_WIDTH-4-1:0]    rd_data;
  wire[(DATA_WIDTH/8)-1:0]  rd_data_parity;
  wire                      rd_data_pipe_fetch;
  reg [31:0]                tx_axis_mac_tdata_word;
  reg [3:0]                 tx_axis_mac_tlast_word;
  wire                      eof_detect;
  reg [1:0]                 rd_data_mux_sel;

  reg                       wr_store_frame_tog  = 0;
  wire                      rd_store_frame_sync;
  reg                       rd_store_frame_d1   = 0;
  wire                      rd_frame_cnt_inc;
  reg                       rd_frame_cnt_dec;
  reg [8:0]                 rd_frames           = 8'h00;

  reg [3:0]                 rd_16_count;
  wire                      rd_txfer_en;
  reg [ADDR_WIDTH-1:0]      rd_addr_txfer;
  reg                       rd_addr_tog;
  reg                       rd_txfer_tog = 0;
  wire                      wr_txfer_tog_sync;
  reg                       wr_txfer_tog_d1;
  wire                      wr_txfer_en;

  reg [ADDR_WIDTH-1:0]      wr_rd_addr          = 0;
  reg [ADDR_WIDTH-1:0]      wr_addr_diff        = 0;
  reg                       wr_fifo_full;
  wire                      fifo_overflow_i;
  reg [3:0]                 wr_fifo_status      = 0;


  // Convert the resets to be active-high
  assign tx_mac_reset  = !tx_mac_resetn;
  assign tx_fifo_reset = !tx_fifo_resetn;

  // Register input signals
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      tx_axis_fifo_tvalid_d1 <= 1'b0;
      tx_axis_fifo_tvalid_d2 <= 1'b0;
    end
    else begin
      tx_axis_fifo_tvalid_d1 <= tx_axis_fifo_tvalid;
      tx_axis_fifo_tvalid_d2 <= tx_axis_fifo_tvalid_d1;
    end
  end

  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      tx_axis_fifo_tdata_d1 <= 0;
    end
    else begin
      tx_axis_fifo_tdata_d1 <= tx_axis_fifo_tdata;
    end
  end

  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      tx_axis_fifo_tlast_d1 <= 1'b0;
    end
    else begin
      tx_axis_fifo_tlast_d1  <= tx_axis_fifo_tlast;
    end
  end

  // tx_axis_fifo_tkeep is used as an frame de-lineator when writing packets to FIFO
  always @(tx_axis_fifo_tkeep)
  begin
    case (tx_axis_fifo_tkeep)
      4'b1111 : tkeep_to_fifo_wr <= 4'b1000;
      4'b0111 : tkeep_to_fifo_wr <= 4'b0100;
      4'b0011 : tkeep_to_fifo_wr <= 4'b0010;
      default : tkeep_to_fifo_wr <= 4'b0001;
    endcase
  end

  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_data_parity <= 0;
    end
    else begin
      wr_data_parity <= {(tx_axis_fifo_tlast && tkeep_to_fifo_wr[3]),
                         (tx_axis_fifo_tlast && tkeep_to_fifo_wr[2]),
                         (tx_axis_fifo_tlast && tkeep_to_fifo_wr[1]),
                         (tx_axis_fifo_tlast && tkeep_to_fifo_wr[0])};
    end
  end

  //-------------------------------------------------------------------------
  // Write Interface: Store the User provided packets to the BRAM for
  // transmission by MAC
  //------------------------------------------------------------------------

  // Write FSM
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_state <= WR_IDLE;
    end
    else begin
      wr_state <= nxt_wr_state;
    end
  end

  // Write FSM next state decode logic (fully combinatorial)
  always @(wr_state, tx_axis_fifo_tvalid_d1, wr_fifo_full, tx_axis_fifo_tlast_d1)
  begin
    case (wr_state)
      WR_IDLE : begin
        if(tx_axis_fifo_tvalid_d1) begin
          if(wr_fifo_full) begin
            nxt_wr_state <= WR_OVERFLOW;
          end
          else begin
            nxt_wr_state <= WRITE_FRAME;
          end
        end
        else begin
          nxt_wr_state <= WR_IDLE;
        end
      end
      WRITE_FRAME : begin
        if(wr_fifo_full) begin
          nxt_wr_state <= WR_OVERFLOW;
        end
        else if(tx_axis_fifo_tlast_d1) begin
          nxt_wr_state <= INC_FRM_CNT;
        end
        else begin
          nxt_wr_state <= WRITE_FRAME;
        end
      end
      WR_OVERFLOW : begin
        if(tx_axis_fifo_tlast_d1) begin
          nxt_wr_state <= RST_WR_ADDRESS;
        end
        else begin
          nxt_wr_state <= WR_OVERFLOW;
        end
      end
      RST_WR_ADDRESS : begin
        nxt_wr_state <= WR_IDLE;
      end
      INC_FRM_CNT : begin
        nxt_wr_state <= WR_IDLE;
      end
      default : begin
        nxt_wr_state <= WR_IDLE;
      end
    endcase
  end

  // FIFO write address logic
  //---------------------------------------------------------------------------------
  // Latch the packet's base address. In case the incomming farme has to be discarded
  // then the write address is reset back to the base address
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_base_addr <= 0;
    end
    else if(wr_state == WR_IDLE) begin
      wr_base_addr <= wr_addr;
    end
  end

  // FIFO write address
  assign wr_addr_inc_en = tx_axis_fifo_tvalid_d1 || tx_axis_fifo_tvalid_d2;

  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_addr <= 0;
    end
    else if(wr_state == RST_WR_ADDRESS) begin
      // Frame is discarded - thus reset the wr_addr to value @ the start of the frame
      // so that the discarded frame can be overwritten with a new frame
      wr_addr <= wr_base_addr;
    end
    else if((wr_state == WRITE_FRAME || wr_state == INC_FRM_CNT) && wr_addr_inc_en) begin
      wr_addr <= wr_addr+1;
    end
  end

  // FIFO write enable
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_en <= 0;
    end
    else if((nxt_wr_state == WRITE_FRAME || wr_state == WRITE_FRAME) && tx_axis_fifo_tvalid_d1) begin
      wr_en <= {(DATA_WIDTH/8) {1'b1}};
    end
    else begin
      wr_en <= {(DATA_WIDTH/8) {1'b0}};
    end
  end

  // FIFO write data
  // Write data pattern is Parity,DataByte => P3B3-P2B2-P1B1-P0B0
  // Parity bit is used for frame de-lineation
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_data <= 0;
    end
    else begin
      wr_data <= {wr_data_parity[3], tx_axis_fifo_tdata_d1[31:24],
                  wr_data_parity[2], tx_axis_fifo_tdata_d1[23:16],
                  wr_data_parity[1], tx_axis_fifo_tdata_d1[15:8],
                  wr_data_parity[0], tx_axis_fifo_tdata_d1[7:0]};
    end
  end

  // tready to user logic
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      tx_axis_fifo_tready_i <= 1'b0;
    end
    else begin
      if(wr_state != WR_OVERFLOW) begin
        tx_axis_fifo_tready_i <= !wr_fifo_full;
      end  
      else begin
        tx_axis_fifo_tready_i <= !(tx_axis_fifo_tlast_d1 && tx_axis_fifo_tvalid_d1);
      end
    end
  end

  //-------------------------------------------------------------------------
  // Read Interface: Read-out the packets stored in FIFO to MAC for transmission
  //------------------------------------------------------------------------

  // Read FSM
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_state <= RD_IDLE;
    end
    else begin
      rd_state <= nxt_rd_state;
    end
  end

  // Read FSM next state decode logic (fully combinatorial)
  always @(rd_state, frame_in_fifo, tx_axis_mac_tready, eof_detect)
  begin
    case (rd_state)
      RD_IDLE : begin
        if(frame_in_fifo) begin
          nxt_rd_state <= WAIT_FOR_RDY;
        end
        else begin
          nxt_rd_state <= RD_IDLE;
        end
      end
      WAIT_FOR_RDY : begin
        if(tx_axis_mac_tready) begin
          nxt_rd_state <= READ_FROM_FIFO;
        end
        else begin
          nxt_rd_state <= WAIT_FOR_RDY;
        end
      end
      READ_FROM_FIFO : begin
        if(eof_detect) begin
          nxt_rd_state <= PREFETCH_RD_DATA_1;
        end
        else begin
          nxt_rd_state <= READ_FROM_FIFO;
        end
      end
      PREFETCH_RD_DATA_1: begin
        nxt_rd_state <= PREFETCH_RD_DATA_2;
      end
      PREFETCH_RD_DATA_2: begin
        nxt_rd_state <= PREFETCH_RD_DATA_3;
      end
      PREFETCH_RD_DATA_3: begin
        nxt_rd_state <= RD_IDLE;
      end
      default : begin
        nxt_rd_state <= RD_IDLE;
      end
    endcase
  end

  // FIFO read address logic
  // The read latency for BRAM is 3-clocks (BRAM o/ps are registered)
  // Latency = 1-clk for rd_addr increment + 2-clks for read data fetch

  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_addr <= 0;
    end
    else if(rd_addr_inc_en) begin
      rd_addr <= rd_addr+1;
    end
  end

  assign rd_addr_inc_en = (rd_data_mux_sel == 2'b00 && tx_axis_mac_tready && tx_axis_mac_tvalid_i) ? 1'b1 : 1'b0;

  // When the FSM is waiting for ACK from the MAC, we should not mux out the next set of data-words
  // to the MAC. This signal will do just that.
  assign rd_data_pipe_fetch = (rd_data_mux_sel == 2'b11 && tx_axis_mac_tready) ? 1'b1 : 1'b0;

  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset || eof_detect) begin
      tx_axis_mac_tlast_word <= 0;
    end
    else if(nxt_rd_state == RD_IDLE || rd_data_pipe_fetch) begin
      tx_axis_mac_tlast_word <= rd_data_parity;
    end
  end

  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      tx_axis_mac_tdata_word <= 0;
    end
    else if(nxt_rd_state == RD_IDLE || rd_data_pipe_fetch) begin
      tx_axis_mac_tdata_word <= rd_data;
    end
  end

  // Mux out the tdata byte to MAC
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset || eof_detect) begin
      rd_data_mux_sel <= 0;
    end
    else if(tx_axis_mac_tvalid_i && tx_axis_mac_tready) begin
      rd_data_mux_sel <= rd_data_mux_sel+1;
    end
  end

  always @(rd_data_mux_sel, tx_axis_mac_tdata_word)
  begin
    case (rd_data_mux_sel)
      2'b11   : tx_axis_mac_tdata_i <= tx_axis_mac_tdata_word[31:24];
      2'b10   : tx_axis_mac_tdata_i <= tx_axis_mac_tdata_word[23:16];
      2'b01   : tx_axis_mac_tdata_i <= tx_axis_mac_tdata_word[15:8];
      default : tx_axis_mac_tdata_i <= tx_axis_mac_tdata_word[7:0];
    endcase
  end

  // Mux out tlast to MAC
  always @(rd_data_mux_sel, tx_axis_mac_tlast_word)
  begin
    case (rd_data_mux_sel)
      2'b11   : tx_axis_mac_tlast_i <= tx_axis_mac_tlast_word[3];
      2'b10   : tx_axis_mac_tlast_i <= tx_axis_mac_tlast_word[2];
      2'b01   : tx_axis_mac_tlast_i <= tx_axis_mac_tlast_word[1];
      default : tx_axis_mac_tlast_i <= tx_axis_mac_tlast_word[0];
    endcase
  end

  // End Of Frame (EOF) detect logic
  assign eof_detect = tx_axis_mac_tlast_i && tx_axis_mac_tready;

  // Data valid logic
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      tx_axis_mac_tvalid_i <= 1'b0;
    end
    else if(nxt_rd_state == WAIT_FOR_RDY || nxt_rd_state == READ_FROM_FIFO) begin
      tx_axis_mac_tvalid_i <= 1'b1;
    end
    else begin
      tx_axis_mac_tvalid_i <= 1'b0;
    end
  end

  //----------------------------------------------------------------------------
  // Frames in FIFO counter logic
  //----------------------------------------------------------------------------
  // We need to know when a frame is stored, in order to increment the count of
  // frames stored in the FIFO.
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_store_frame_tog <= 1'b0;
    end
    else if(nxt_wr_state == INC_FRM_CNT) begin
      wr_store_frame_tog <= !wr_store_frame_tog;
    end
  end

  // The frame available counter is in Read Clock domain. Thus synchronize wr_store_frame_tog
  // to Read clock domain
  tri_mode_ethernet_mac_0_sync_block resync_wr_store_frame_tog (
    .clk       (tx_mac_aclk),
    .data_in   (wr_store_frame_tog),
    .data_out  (rd_store_frame_sync)
  );

  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_store_frame_d1 <= 1'b0;
    end
    else begin
      rd_store_frame_d1 <= rd_store_frame_sync;
    end
  end

  assign rd_frame_cnt_inc = rd_store_frame_sync ^ rd_store_frame_d1;

  // We need to know when a frame is being read-out from the FIFO (Completly Pulled), in order to decrement the count of
  // frames stored in the FIFO.
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_frame_cnt_dec <= 1'b0;
    end
    else if(nxt_rd_state == PREFETCH_RD_DATA_1 && rd_state == READ_FROM_FIFO) begin
      rd_frame_cnt_dec <= 1'b1;
    end
    else begin
      rd_frame_cnt_dec <= 1'b0;
    end
  end

  // Up/down counter to monitor the number of frames stored within the FIFO.
  // Note:
  //    * increments at the end of a frame write cycle
  //    * decrements at the beginning of a frame read cycle
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_frames <= 0;
    end
    else begin
      // A frame is written to the FIFO in this cycle, and no frame is being
      // read out on the same cycle.
      if(rd_frame_cnt_inc && !rd_frame_cnt_dec) begin
        rd_frames <= rd_frames+1;
      // A frame is being read out on this cycle and no frame is being
      // written on the same cycle.
      end
      else if(!rd_frame_cnt_inc && rd_frame_cnt_dec) begin
        rd_frames <= rd_frames-1;
      end
    end
  end

  //** Modified by Abhay
  //**assign frame_in_fifo = (rd_frames != 0) ? 1'b1 : 1'b0;
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      frame_in_fifo <= 1'b0;
    end
    else begin
      if(rd_frames != 0) begin
        frame_in_fifo <= 1'b1;
      end
      else begin
        frame_in_fifo <= 1'b0;
      end
    end
  end  

  //----------------------------------------------------------------------------
  // FIFO full functionality
  //----------------------------------------------------------------------------

  // Full functionality is the difference between read and write addresses.
  // We cannot use gray code this time as the read address and read start
  // addresses jump by more than 1.
  // We generate an enable pulse for the read side every 16 read clocks.
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_16_count <= 0;
    end
    else begin
      rd_16_count <= rd_16_count+1;
    end
  end

  assign rd_txfer_en = (rd_16_count == 4'hF) ? 1'b1 : 1'b0;

  // Register the start address on the enable pulse.
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_addr_txfer <= 0;
    end
    else if(rd_txfer_en) begin
      rd_addr_txfer <= rd_addr;
    end
  end

  // Generate a toggle to indicate that the address has been loaded.
  always @(posedge tx_mac_aclk)
  begin
    if(tx_mac_reset) begin
      rd_txfer_tog <= 1'b0;
    end
    else if(rd_txfer_en) begin
      rd_txfer_tog <= !rd_txfer_tog;
    end
  end

  // Synchronize the toggle to the write side.
  tri_mode_ethernet_mac_0_sync_block resync_rd_txfer_tog (
    .clk       (tx_fifo_aclk),
    .data_in   (rd_txfer_tog),
    .data_out  (wr_txfer_tog_sync)
  );

  // Delay the synchronized toggle by one cycle.
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_txfer_tog_d1 <= 1'b0;
    end
    else begin
      wr_txfer_tog_d1 <= wr_txfer_tog_sync;
    end
  end

  // Generate an enable pulse from the toggle. The address should have been
  // steady on the wr clock input for at least one clock.
  assign wr_txfer_en = wr_txfer_tog_d1 ^ wr_txfer_tog_sync;

  // Capture the address on the write clock when the enable pulse is high.
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_rd_addr <= 0;
    end
    else if(wr_txfer_en) begin
      wr_rd_addr <= rd_addr_txfer;
    end
  end

  // Obtain the difference between write and read pointers
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_addr_diff <= 0;
    end
    else begin
      wr_addr_diff <= wr_rd_addr-wr_addr;
    end
  end

  // Detect when the FIFO is full.
  // The FIFO is considered to be full if the write address pointer is
  // within 0 to 3 of the read address pointer.
  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_fifo_full <= 1'b0;
    end
    else begin
      if(wr_addr_diff[ADDR_WIDTH-1:4] == 0 & wr_addr_diff[3:2] != 0) begin
        wr_fifo_full <= 1'b1;
      end
      else begin
        wr_fifo_full <= 1'b0;
      end
    end
  end

  //----------------------------------------------------------------------------
  // FIFO status signals
  //----------------------------------------------------------------------------
  // The FIFO status is four bits which represents the occupancy of the FIFO
  // in sixteenths. To generate this signal we therefore only need to compare
  // the 4 most significant bits of the write address pointer with the 4 most
  // significant bits of the read address pointer.
  assign fifo_overflow_i = (wr_state == WR_OVERFLOW) ? 1'b1 : 1'b0;

  always @(posedge tx_fifo_aclk)
  begin
    if(tx_fifo_reset) begin
      wr_fifo_status <= 0;
    end
    else begin
      if(wr_addr_diff == 0) begin
        wr_fifo_status <= 4'b0000;
      end
      else begin
        wr_fifo_status[3] <= !wr_addr_diff[ADDR_WIDTH-1];
        wr_fifo_status[2] <= !wr_addr_diff[ADDR_WIDTH-2];
        wr_fifo_status[1] <= !wr_addr_diff[ADDR_WIDTH-3];
        wr_fifo_status[0] <= !wr_addr_diff[ADDR_WIDTH-4];
      end
    end
  end

  assign fifo_status = wr_fifo_status;

  //----------------------------------------------------------------------------
  // Instantiate Block RAM
  //----------------------------------------------------------------------------
  tri_mode_ethernet_mac_0_bram_tdp_2g5 # (
    .ADDR_WIDTH    (ADDR_WIDTH),
    .DATA_WIDTH    (DATA_WIDTH),
    .REGISTER_OP   (1)
  )
  tx_bram (
    // Port A: Write Packets
    .a_clk  (tx_fifo_aclk),
    .a_rst  (tx_fifo_reset),
    .a_addr (wr_addr),
    .a_be   (wr_en),
    .a_din  (wr_data),
    // Port B: Read Packets
    .b_clk  (tx_mac_aclk),
    .b_rst  (tx_mac_reset),
    .b_addr (rd_addr),
    .b_dout (rd_data_with_parity)
  );

  assign rd_data = {rd_data_with_parity[DATA_WIDTH-2 :DATA_WIDTH-9]  ,
                    rd_data_with_parity[DATA_WIDTH-11:DATA_WIDTH-18] ,
                    rd_data_with_parity[DATA_WIDTH-20:DATA_WIDTH-27] ,
                    rd_data_with_parity[DATA_WIDTH-29:DATA_WIDTH-36]};

  assign rd_data_parity[3] = tx_axis_mac_tvalid_i && rd_data_with_parity[DATA_WIDTH-1];
  assign rd_data_parity[2] = tx_axis_mac_tvalid_i && rd_data_with_parity[DATA_WIDTH-10];
  assign rd_data_parity[1] = tx_axis_mac_tvalid_i && rd_data_with_parity[DATA_WIDTH-19];
  assign rd_data_parity[0] = tx_axis_mac_tvalid_i && rd_data_with_parity[DATA_WIDTH-28];

  //----------------------------------------------------------------------------
  // Assign to o/p ports
  //----------------------------------------------------------------------------
  assign tx_axis_fifo_tready = tx_axis_fifo_tready_i;
  assign tx_axis_mac_tvalid  = tx_axis_mac_tvalid_i;
  assign tx_axis_mac_tdata   = tx_axis_mac_tdata_i;
  assign tx_axis_mac_tlast   = tx_axis_mac_tlast_i;
  assign tx_axis_mac_tuser   = 1'b0;
  assign fifo_overflow       = fifo_overflow_i;

endmodule
